# -*- coding: utf-8 -*-
# Copyright (C) 2022 ralf.klammer@tu-dresden.de
import logging

log = logging.getLogger(__name__)


def cli_startup(log_level=logging.INFO, log_file=None):
    log_config = dict(
        level=log_level,
        format='%(asctime)s %(name)-10s %(levelname)-4s %(message)s',)
    if log_file:
        log_config['filename'] = log_file

    logging.basicConfig(**log_config)
    logging.getLogger('').setLevel(log_level)
