# -*- coding: utf-8 -*-
# Copyright (C) 2022 ralf.klammer@tu-dresden.de
import logging

import click

from .util import cli_startup

log = logging.getLogger(__name__)


@click.group()
@click.option('--debug/--no-debug', '-d', is_flag=True, default=False)
@click.pass_context
def main(ctx, debug, login):
    '''
    This is quite a simple dummy project, to start from...
    '''
    cli_startup(log_level=debug and logging.DEBUG or logging.INFO)
    ctx.ensure_object(dict)
    ctx.obj['DEBUG'] = debug


@main.command()
@click.pass_context
def test(ctx, **kw):
    log.info('Lets go!')

if __name__ == '__main__':
    main(obj={})
