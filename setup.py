# -*- coding: utf-8 -*-
# Copyright (C) 2022 ralf.klammer@tu-dresden.de

from setuptools import setup

setup(
    name='dummy_project',
    version='0.1.0',
    description='A simple starting point for a new python project',
    author='Ralf Klammer',
    author_email='ralf.klammer@tu-dresden.de',
    packages=['dummy_project'],
    install_requires=[
        'click'
    ],
    package_data={
        'dummy_project': [],
    },
    entry_points={
        'console_scripts': [
            'dummy_project = dummy_project.cli:main',
        ]
    }
)
